export const fakeDatabase = {
  collection: [{
    id: 0,
    name: 'fur',
    itemId: 'gcat',
  }, {
    id: 1,
    name: 'tongue',
    itemId: 'llizard',
  }],
};
const delay = (ms) =>
  new Promise(resolve => window.setTimeout(resolve, ms));
export const getCollection = () =>
  delay(2000).then(() =>
    fakeDatabase.collection.map(o => ({ ...o }))
    // throw new Error('500');
  );
export const post = (element) =>
  delay(2000).then(() => {
    // throw new Error('500'); // SERVER ERROR
    const newElement = element;
    newElement.id = Date.now();
    fakeDatabase.collection.push({ ...newElement });
    return { ...newElement };
  });
export const put = (id, updates) =>
  delay(2000).then(() => {
    // throw new Error('500'); // SERVER ERROR
    const element = fakeDatabase.collection.find(o => o.id === id);
    if (element === undefined) {
      throw new Error('404'); // MISSING ID
    }
    Object.assign(element, updates);
    return { ...element };
  });
export const del = (id) =>
  delay(2000).then(() => {
    // throw new Error('500'); // SERVER ERROR
    const index = fakeDatabase.collection.findIndex(o => o.id === id);
    if (index === -1) throw new Error('404'); // MISSING ID
    const deleted = fakeDatabase.collection.splice(index, 1)[0];
    return { ...deleted };
  });

import * as fromParts from './parts';

const fakeDatabase = {
  collection: [{
    id: 'gcat',
    description: 'grumpy cat',
  }, {
    id: 'sdog',
    description: 'super dog',
  }, {
    id: 'cbird',
    description: 'chirpy bird',
  }, {
    id: 'llizard',
    description: 'lounging lizard',
  }],
};
const delay = (ms) =>
  new Promise(resolve => window.setTimeout(resolve, ms));
export const getCollection = () =>
  delay(2000).then(() =>
    fakeDatabase.collection.map(o => ({ ...o }))
    // throw new Error('500');
  );
export const post = (element) =>
  delay(2000).then(() => {
    // throw new Error('500'); // SERVER ERROR
    if (fakeDatabase.collection.find(o => o.id === element.id) !== undefined) {
      throw new Error('409'); // CONFLICTING ID
    }
    fakeDatabase.collection.push({ ...element });
    return { ...element };
  });
export const put = (id, updates) =>
  delay(2000).then(() => {
    // throw new Error('500'); // SERVER ERROR
    const element = fakeDatabase.collection.find(o => o.id === id);
    if (element === undefined) {
      throw new Error('404'); // MISSING ID
    }
    Object.assign(element, updates);
    return { ...element };
  });
export const del = (id) =>
  delay(2000).then(() => {
    // throw new Error('500'); // SERVER ERROR
    const index = fakeDatabase.collection.findIndex(o => o.id === id);
    if (index === -1) throw new Error('404'); // MISSING ID
    const partsFakeDatabase = fromParts.fakeDatabase;
    const filtered = partsFakeDatabase.collection.filter(o => o.itemId === id);
    for (let i = 0; i < filtered.length; i++) {
      const part = filtered[i];
      const partIndex = partsFakeDatabase.collection.findIndex(o => o.id === part.id);
      partsFakeDatabase.collection.splice(partIndex, 1);
    }
    const deleted = fakeDatabase.collection.splice(index, 1)[0];
    return { ...deleted };
  });

import { combineReducers } from 'redux';
import { reducer as formReducer } from 'redux-form';
import { routerReducer } from 'react-router-redux';
import appBlocking from '../ducks/appBlocking';
import routesBlocking from '../ducks/routesBlocking';
import items from '../ducks/items';
import parts from '../ducks/parts';
import snackbars from '../ducks/snackbars';

export default combineReducers({
  form: formReducer,
  routing: routerReducer,
  appBlocking,
  routesBlocking,
  items,
  parts,
  snackbars,
});

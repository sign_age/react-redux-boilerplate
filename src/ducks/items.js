import { combineReducers } from 'redux';
import { normalize, schema } from 'normalizr';
import { createSelector } from 'reselect';
import { ACTION_PREFIX } from '../strings';
import { ServerException } from '../util/exceptions';
import { addPart, getParts, removePartLocal } from './parts';
// API
import { del, getCollection, post, put } from '../apis/items';

// REDUCER MOUNT POINT
const reducerMountPoint = 'items';
// ACTIONS
export const FETCH_ITEMS_REQUEST = `${ACTION_PREFIX}FETCH_ITEMS_REQUEST`;
export const FETCH_ITEMS_SUCCESS = `${ACTION_PREFIX}FETCH_ITEMS_SUCCESS`;
export const FETCH_ITEMS_ERROR = `${ACTION_PREFIX}FETCH_ITEMS_ERROR`;
export const RESET_FETCH_ITEMS_ERROR = `${ACTION_PREFIX}RESET_FETCH_ITEMS_ERROR`;
export const ADD_ITEM_REQUEST = `${ACTION_PREFIX}ADD_ITEM_REQUEST`;
export const ADD_ITEM_SUCCESS = `${ACTION_PREFIX}ADD_ITEM_SUCCESS`;
export const ADD_ITEM_ERROR = `${ACTION_PREFIX}ADD_ITEM_ERROR`;
export const RESET_ADD_ITEM_ERROR = `${ACTION_PREFIX}RESET_ADD_ITEM_ERROR`;
export const UPDATE_ITEM_REQUEST = `${ACTION_PREFIX}UPDATE_ITEM_REQUEST`;
export const UPDATE_ITEM_SUCCESS = `${ACTION_PREFIX}UPDATE_ITEM_SUCCESS`;
export const UPDATE_ITEM_ERROR = `${ACTION_PREFIX}UPDATE_ITEM_ERROR`;
export const RESET_UPDATE_ITEM_ERROR = `${ACTION_PREFIX}RESET_UPDATE_ITEM_ERROR`;
export const REMOVE_ITEM_REQUEST = `${ACTION_PREFIX}REMOVE_ITEM_REQUEST`;
export const REMOVE_ITEM_SUCCESS = `${ACTION_PREFIX}REMOVE_ITEM_SUCCESS`;
export const REMOVE_ITEM_ERROR = `${ACTION_PREFIX}REMOVE_ITEM_ERROR`;
export const RESET_REMOVE_ITEM_ERROR = `${ACTION_PREFIX}RESET_REMOVE_ITEM_ERROR`;
// SCHEMA
const itemSchema = new schema.Entity('items');
const itemsSchema = new schema.Array(itemSchema);
// REDUCERS
const byId = (state = {}, action) => {
  switch (action.type) {
    case FETCH_ITEMS_SUCCESS:
    case ADD_ITEM_SUCCESS:
    case UPDATE_ITEM_SUCCESS: {
      return {
        ...state,
        ...action.response.entities.items,
      };
    }
    case REMOVE_ITEM_SUCCESS: {
      const newState = { ...state };
      delete newState[action.response.result];
      return newState;
    }
    default:
      return state;
  }
};
const ids = (state = [], action) => {
  switch (action.type) {
    case FETCH_ITEMS_SUCCESS:
      return action.response.result;
    case ADD_ITEM_SUCCESS:
      return [...state, action.response.result];
    case REMOVE_ITEM_SUCCESS: {
      const newState = [...state];
      newState.splice(
        state.indexOf(action.response.result),
        1
      );
      return newState;
    }
    default:
      return state;
  }
};
const isAsync = (state = false, action) => {
  switch (action.type) {
    case FETCH_ITEMS_REQUEST:
    case ADD_ITEM_REQUEST:
    case UPDATE_ITEM_REQUEST:
    case REMOVE_ITEM_REQUEST:
      return true;
    case FETCH_ITEMS_SUCCESS:
    case ADD_ITEM_SUCCESS:
    case UPDATE_ITEM_SUCCESS:
    case REMOVE_ITEM_SUCCESS:
    case FETCH_ITEMS_ERROR:
    case ADD_ITEM_ERROR:
    case UPDATE_ITEM_ERROR:
    case REMOVE_ITEM_ERROR:
      return false;
    default:
      return state;
  }
};
const asyncErrorMessage = (state = null, action) => {
  switch (action.type) {
    case FETCH_ITEMS_ERROR:
    case ADD_ITEM_ERROR:
    case UPDATE_ITEM_ERROR:
    case REMOVE_ITEM_ERROR:
      return action.message;
    case FETCH_ITEMS_REQUEST:
    case FETCH_ITEMS_SUCCESS:
    case RESET_FETCH_ITEMS_ERROR:
    case ADD_ITEM_REQUEST:
    case ADD_ITEM_SUCCESS:
    case RESET_ADD_ITEM_ERROR:
    case UPDATE_ITEM_REQUEST:
    case UPDATE_ITEM_SUCCESS:
    case RESET_UPDATE_ITEM_ERROR:
    case REMOVE_ITEM_REQUEST:
    case REMOVE_ITEM_SUCCESS:
    case RESET_REMOVE_ITEM_ERROR:
      return null;
    default:
      return state;
  }
};
const lastAsync = (state = null, action) => {
  switch (action.type) {
    case FETCH_ITEMS_REQUEST:
      return 'fetch';
    case ADD_ITEM_REQUEST:
      return 'add';
    case UPDATE_ITEM_REQUEST:
      return 'update';
    case REMOVE_ITEM_REQUEST:
      return 'remove';
    default:
      return state;
  }
};
export default combineReducers({
  byId,
  ids,
  isAsync,
  asyncErrorMessage,
  lastAsync,
});
// ACCESSORS AKA SELECTORS
const getIsAsync = (state) => state[reducerMountPoint].isAsync;
const getLastAsync = (state) => state[reducerMountPoint].lastAsync;
export const getItem = (state, id) => state[reducerMountPoint].byId[id];
const getItemsIds = state => state[reducerMountPoint].ids;
const getItemsById = state => state[reducerMountPoint].byId;
export const getItems = createSelector(
  [getItemsIds, getItemsById],
  (itemsIds, itemsById) => itemsIds.map(id => itemsById[id])
);
export const getIsFetchingItems = (state) =>
  getLastAsync(state) === 'fetch' && getIsAsync(state);
export const getFetchItemsErrorMessage = (state) => (
  getLastAsync(state) === 'fetch' ? state[reducerMountPoint].asyncErrorMessage : null);
export const getIsAddingItem = (state) =>
  getLastAsync(state) === 'add' && getIsAsync(state);
export const getAddItemErrorMessage = (state) => (
  getLastAsync(state) === 'add' ? state[reducerMountPoint].asyncErrorMessage : null);
export const getIsUpdatingItem = (state) =>
  getLastAsync(state) === 'update' && getIsAsync(state);
export const getUpdateItemErrorMessage = (state) => (
  getLastAsync(state) === 'update' ? state[reducerMountPoint].asyncErrorMessage : null);
export const getIsRemovingItem = (state) =>
  getLastAsync(state) === 'remove' && getIsAsync(state);
export const getRemoveItemErrorMessage = (state) => (
  getLastAsync(state) === 'remove' ? state[reducerMountPoint].asyncErrorMessage : null);
const passItemId = (_, itemId) => itemId;
export const getItemParts = createSelector(
  [getParts, passItemId],
  (parts, itemId) => parts.filter(o => o.itemId === itemId)
);
// ACTION CREATOR VALIDATORS
const validItem = (item) =>
  !(item === undefined
  || item.id === undefined
  || typeof item.id !== 'string'
  || item.id === ''
  || item.description === undefined
  || typeof item.description !== 'string');
const validNewItem = (state, item) =>
  validItem(item) && getItem(state, item.id) === undefined;
const validExistingItem = (state, item) =>
  validItem(item) && getItem(state, item.id) !== undefined;
// ACTION CREATORS
export const fetchItems = () => (dispatch, getState) => {
  if (getIsAsync(getState())) throw new Error();
  dispatch({
    type: FETCH_ITEMS_REQUEST,
  });
  return getCollection()
    .then(
      response => dispatch({
        type: FETCH_ITEMS_SUCCESS,
        response: normalize(response, itemsSchema),
      }),
      error => {
        dispatch({
          type: FETCH_ITEMS_ERROR,
          message: error.message,
        });
        throw new ServerException(error.message);
      }
    );
};
export const resetFetchItemsError = () => ({
  type: RESET_FETCH_ITEMS_ERROR,
});
export const addItemLocal = (item) => ({
  type: ADD_ITEM_SUCCESS,
  response: normalize(item, itemSchema),
});
export const addItem = (item) => (dispatch, getState) => {
  const state = getState();
  if (getIsAsync(state)) throw new Error();
  if (!validItem(item)) throw new Error();
  if (!validNewItem(state, item)) {
    return Promise.reject(new ServerException('409'));
  }
  dispatch({
    type: ADD_ITEM_REQUEST,
    item,
  });
  return post(item)
    .then(
      response => {
        dispatch(addItemLocal(response));
      },
      error => {
        dispatch({
          type: ADD_ITEM_ERROR,
          message: error.message,
        });
        throw new ServerException(error.message);
      }
    );
};
export const resetAddItemError = () => ({
  type: RESET_ADD_ITEM_ERROR,
});
export const updateItemLocal = (item) => ({
  type: UPDATE_ITEM_SUCCESS,
  response: normalize(item, itemSchema),
});
export const updateItem = (item) => (dispatch, getState) => {
  const state = getState();
  if (getIsAsync(state)) throw new Error();
  if (!validExistingItem(state, item)) throw new Error();
  dispatch({
    type: UPDATE_ITEM_REQUEST,
    item,
  });
  return put(item.id, item)
  .then(
      response => {
        dispatch(updateItemLocal(response));
      },
      error => {
        dispatch({
          type: UPDATE_ITEM_ERROR,
          message: error.message,
        });
        throw new ServerException(error.message);
      }
    );
};
export const resetUpdateItemError = () => ({
  type: RESET_UPDATE_ITEM_ERROR,
});
export const removeItemLocal = (id) => (dispatch, getState) => {
  const state = getState();
  const item = getItem(state, id);
  if (!validExistingItem(state, item)) throw new Error();
  dispatch({
    type: REMOVE_ITEM_SUCCESS,
    response: normalize(item, itemSchema),
  });
};
export const removeItem = (id) => (dispatch, getState) => {
  const state = getState();
  const item = getItem(state, id);
  if (getIsAsync(state)) throw new Error();
  if (!validExistingItem(state, item)) throw new Error();
  dispatch({
    type: REMOVE_ITEM_REQUEST,
    item,
  });
  return del(item.id)
    .then(
      () => {
        const itemParts = getItemParts(state, item.id);
        for (let i = 0; i < itemParts.length; i++) {
          removePartLocal(itemParts[i].id)(dispatch, getState);
        }
        removeItemLocal(item.id)(dispatch, getState);
      },
      error => {
        dispatch({
          type: REMOVE_ITEM_ERROR,
          message: error.message,
        });
        throw new ServerException(error.message);
      }
    );
};
export const resetRemoveItemError = () => ({
  type: RESET_REMOVE_ITEM_ERROR,
});
export const addItemPart = (item, part) => (dispatch, getState) => {
  const state = getState();
  const newPart = part;
  if (!validExistingItem(state, item)) throw new Error();
  newPart.itemId = item.id;
  return addPart(newPart)(dispatch, getState);
};

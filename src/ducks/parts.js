import { combineReducers } from 'redux';
import { normalize, schema } from 'normalizr';
import { createSelector } from 'reselect';
import { ACTION_PREFIX } from '../strings';
import { ServerException } from '../util/exceptions';
// API
import { del, getCollection, post, put } from '../apis/parts';

// REDUCER MOUNT POINT
const reducerMountPoint = 'parts';
// ACTIONS
export const FETCH_PARTS_REQUEST = `${ACTION_PREFIX}FETCH_PARTS_REQUEST`;
export const FETCH_PARTS_SUCCESS = `${ACTION_PREFIX}FETCH_PARTS_SUCCESS`;
export const FETCH_PARTS_ERROR = `${ACTION_PREFIX}FETCH_PARTS_ERROR`;
export const RESET_FETCH_PARTS_ERROR = `${ACTION_PREFIX}RESET_FETCH_PARTS_ERROR`;
export const ADD_PART_REQUEST = `${ACTION_PREFIX}ADD_PART_REQUEST`;
export const ADD_PART_SUCCESS = `${ACTION_PREFIX}ADD_PART_SUCCESS`;
export const ADD_PART_ERROR = `${ACTION_PREFIX}ADD_PART_ERROR`;
export const RESET_ADD_PART_ERROR = `${ACTION_PREFIX}RESET_ADD_PART_ERROR`;
export const UPDATE_PART_REQUEST = `${ACTION_PREFIX}UPDATE_PART_REQUEST`;
export const UPDATE_PART_SUCCESS = `${ACTION_PREFIX}UPDATE_PART_SUCCESS`;
export const UPDATE_PART_ERROR = `${ACTION_PREFIX}UPDATE_PART_ERROR`;
export const RESET_UPDATE_PART_ERROR = `${ACTION_PREFIX}RESET_UPDATE_PART_ERROR`;
export const REMOVE_PART_REQUEST = `${ACTION_PREFIX}REMOVE_PART_REQUEST`;
export const REMOVE_PART_SUCCESS = `${ACTION_PREFIX}REMOVE_PART_SUCCESS`;
export const REMOVE_PART_ERROR = `${ACTION_PREFIX}REMOVE_PART_ERROR`;
export const RESET_REMOVE_PART_ERROR = `${ACTION_PREFIX}RESET_REMOVE_PART_ERROR`;
// SCHEMA
const partSchema = new schema.Entity('parts');
const partsSchema = new schema.Array(partSchema);
// REDUCERS
const byId = (state = {}, action) => {
  switch (action.type) {
    case FETCH_PARTS_SUCCESS:
    case ADD_PART_SUCCESS:
    case UPDATE_PART_SUCCESS: {
      return {
        ...state,
        ...action.response.entities.parts,
      };
    }
    case REMOVE_PART_SUCCESS: {
      const newState = { ...state };
      delete newState[action.response.result];
      return newState;
    }
    default:
      return state;
  }
};
const ids = (state = [], action) => {
  switch (action.type) {
    case FETCH_PARTS_SUCCESS:
      return action.response.result;
    case ADD_PART_SUCCESS:
      return [...state, action.response.result];
    case REMOVE_PART_SUCCESS: {
      const newState = [...state];
      newState.splice(
        state.indexOf(action.response.result),
        1
      );
      return newState;
    }
    default:
      return state;
  }
};
const isAsync = (state = false, action) => {
  switch (action.type) {
    case FETCH_PARTS_REQUEST:
    case ADD_PART_REQUEST:
    case UPDATE_PART_REQUEST:
    case REMOVE_PART_REQUEST:
      return true;
    case FETCH_PARTS_SUCCESS:
    case ADD_PART_SUCCESS:
    case UPDATE_PART_SUCCESS:
    case REMOVE_PART_SUCCESS:
    case FETCH_PARTS_ERROR:
    case ADD_PART_ERROR:
    case UPDATE_PART_ERROR:
    case REMOVE_PART_ERROR:
      return false;
    default:
      return state;
  }
};
const asyncErrorMessage = (state = null, action) => {
  switch (action.type) {
    case FETCH_PARTS_ERROR:
    case ADD_PART_ERROR:
    case UPDATE_PART_ERROR:
    case REMOVE_PART_ERROR:
      return action.message;
    case FETCH_PARTS_REQUEST:
    case FETCH_PARTS_SUCCESS:
    case RESET_FETCH_PARTS_ERROR:
    case ADD_PART_REQUEST:
    case ADD_PART_SUCCESS:
    case RESET_ADD_PART_ERROR:
    case UPDATE_PART_REQUEST:
    case UPDATE_PART_SUCCESS:
    case RESET_UPDATE_PART_ERROR:
    case REMOVE_PART_REQUEST:
    case REMOVE_PART_SUCCESS:
    case RESET_REMOVE_PART_ERROR:
      return null;
    default:
      return state;
  }
};
const lastAsync = (state = null, action) => {
  switch (action.type) {
    case FETCH_PARTS_REQUEST:
      return 'fetch';
    case ADD_PART_REQUEST:
      return 'add';
    case UPDATE_PART_REQUEST:
      return 'update';
    case REMOVE_PART_REQUEST:
      return 'remove';
    default:
      return state;
  }
};
export default combineReducers({
  byId,
  ids,
  isAsync,
  asyncErrorMessage,
  lastAsync,
});
// ACCESSORS AKA SELECTORS
const getIsAsync = (state) => state[reducerMountPoint].isAsync;
const getLastAsync = (state) => state[reducerMountPoint].lastAsync;
export const getPart = (state, id) => state[reducerMountPoint].byId[id];
const getPartsIds = state => state[reducerMountPoint].ids;
const getPartsById = state => state[reducerMountPoint].byId;
export const getParts = createSelector(
  [getPartsIds, getPartsById],
  (partsIds, partsById) => partsIds.map(id => partsById[id])
);
export const getIsFetchingParts = (state) =>
  getLastAsync(state) === 'fetch' && getIsAsync(state);
export const getFetchPartsErrorMessage = (state) => (
  getLastAsync(state) === 'fetch' ? state[reducerMountPoint].asyncErrorMessage : null);
export const getIsAddingPart = (state) =>
  getLastAsync(state) === 'add' && getIsAsync(state);
export const getAddPartErrorMessage = (state) => (
  getLastAsync(state) === 'add' ? state[reducerMountPoint].asyncErrorMessage : null);
export const getIsUpdatingPart = (state) =>
  getLastAsync(state) === 'update' && getIsAsync(state);
export const getUpdatePartErrorMessage = (state) => (
  getLastAsync(state) === 'update' ? state[reducerMountPoint].asyncErrorMessage : null);
export const getIsRemovingPart = (state) =>
  getLastAsync(state) === 'remove' && getIsAsync(state);
export const getRemovePartErrorMessage = (state) => (
  getLastAsync(state) === 'remove' ? state[reducerMountPoint].asyncErrorMessage : null);
// ACTION CREATOR VALIDATORS
const validNewPart = (state, part) =>
  !(part === undefined
  || part.name === undefined
  || typeof part.name !== 'string');
const validExistingPart = (state, part) =>
  validNewPart(state, part) && getPart(state, part.id) !== undefined;
// ACTION CREATORS
export const fetchParts = () => (dispatch, getState) => {
  if (getIsAsync(getState())) throw new Error();
  dispatch({
    type: FETCH_PARTS_REQUEST,
  });
  return getCollection()
    .then(
      response => dispatch({
        type: FETCH_PARTS_SUCCESS,
        response: normalize(response, partsSchema),
      }),
      error => {
        dispatch({
          type: FETCH_PARTS_ERROR,
          message: error.message,
        });
        throw new ServerException(error.message);
      }
    );
};
export const resetFetchPartsError = () => ({
  type: RESET_FETCH_PARTS_ERROR,
});
export const addPartLocal = (part) => ({
  type: ADD_PART_SUCCESS,
  response: normalize(part, partSchema),
});
export const addPart = (part) => (dispatch, getState) => {
  const state = getState();
  if (getIsAsync(state)) throw new Error();
  if (!validNewPart(state, part)) throw new Error();
  dispatch({
    type: ADD_PART_REQUEST,
    part,
  });
  return post(part)
    .then(
      response => {
        dispatch(addPartLocal(response));
      },
      error => {
        dispatch({
          type: ADD_PART_ERROR,
          message: error.message,
        });
        throw new ServerException(error.message);
      }
    );
};
export const resetAddPartError = () => ({
  type: RESET_ADD_PART_ERROR,
});
export const updatePartLocal = (part) => ({
  type: UPDATE_PART_SUCCESS,
  response: normalize(part, partSchema),
});
export const updatePart = (part) => (dispatch, getState) => {
  const state = getState();
  if (getIsAsync(state)) throw new Error();
  if (!validExistingPart(state, part)) throw new Error();
  dispatch({
    type: UPDATE_PART_REQUEST,
    part,
  });
  return put(part.id, part)
  .then(
      response => {
        dispatch(updatePartLocal(response));
      },
      error => {
        dispatch({
          type: UPDATE_PART_ERROR,
          message: error.message,
        });
        throw new ServerException(error.message);
      }
    );
};
export const resetUpdatePartError = () => ({
  type: RESET_UPDATE_PART_ERROR,
});
export const removePartLocal = (id) => (dispatch, getState) => {
  const state = getState();
  const part = getPart(state, id);
  if (!validExistingPart(state, part)) throw new Error();
  dispatch({
    type: REMOVE_PART_SUCCESS,
    response: normalize(part, partSchema),
  });
};
export const removePart = (id) => (dispatch, getState) => {
  const state = getState();
  const part = getPart(state, id);
  if (getIsAsync(state)) throw new Error();
  if (!validExistingPart(state, part)) throw new Error();
  dispatch({
    type: REMOVE_PART_REQUEST,
    part,
  });
  return del(part.id)
    .then(
      () => {
        removePartLocal(part.id)(dispatch, getState);
      },
      error => {
        dispatch({
          type: REMOVE_PART_ERROR,
          message: error.message,
        });
        throw new ServerException(error.message);
      }
    );
};
export const resetRemovePartError = () => ({
  type: RESET_REMOVE_PART_ERROR,
});

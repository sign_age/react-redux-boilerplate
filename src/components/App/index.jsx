import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';
import * as fromAppBlocking from '../../ducks/appBlocking';
import * as fromItems from '../../ducks/items';
import * as fromParts from '../../ducks/parts';
import Blocking from '../Blocking';
import AppError from './AppError';
import Routes from '../Routes';

class App extends Component {
  // HACK: USE REACT STATE TO PREVENT MIS-RENDER ON FIRST LOAD
  componentWillMount() {
    this.setState({ initialProps: true });
  }
  componentDidMount() {
    const { fetchItems, fetchParts, setAppBlocking } = this.props;
    Promise.all([
      fetchItems(),
      fetchParts(),
    ])
      .then(
        () => {
          // COMPONENT CONTROLLER HANDLE SUCCESS BLOCK
          setAppBlocking(false);
        },
        (error) => {
          setAppBlocking(false);
          // DEBUGGING TOOL
          if (process.env.NODE_ENV !== 'production'
            && error.name !== 'ServerException') {
            window.console.log(error);
            return;
          }
          // COMPONENT CONTROLLER HANDLE ERROR BLOCK
        }
      );
  }
  componentWillReceiveProps() {
    this.setState({ initialProps: false });
  }
  render() {
    const { initialProps } = this.state;
    const {
      appBlocking,
      fetchItemsErrorMessage,
      fetchPartsErrorMessage,
      isFetchingItems,
      isFetchingParts,
    } = this.props;
    if (appBlocking) return <Blocking />;
    if (initialProps) return null;
    if (isFetchingItems || isFetchingParts) return null;
    if (fetchItemsErrorMessage !== null || fetchPartsErrorMessage !== null) return <AppError />;
    return <Routes />;
  }
}
App.propTypes = {
  appBlocking: PropTypes.bool.isRequired,
  fetchItems: PropTypes.func.isRequired,
  fetchParts: PropTypes.func.isRequired,
  fetchItemsErrorMessage: PropTypes.string,
  fetchPartsErrorMessage: PropTypes.string,
  isFetchingItems: PropTypes.bool.isRequired,
  isFetchingParts: PropTypes.bool.isRequired,
  setAppBlocking: PropTypes.func.isRequired,
};
export default connect(
  (state) => ({
    appBlocking: fromAppBlocking.getAppBlocking(state),
    fetchItemsErrorMessage: fromItems.getFetchItemsErrorMessage(state),
    fetchPartsErrorMessage: fromParts.getFetchPartsErrorMessage(state),
    isFetchingItems: fromItems.getIsFetchingItems(state),
    isFetchingParts: fromParts.getIsFetchingParts(state),
  }),
  {
    fetchItems: fromItems.fetchItems,
    fetchParts: fromParts.fetchParts,
    setAppBlocking: fromAppBlocking.setAppBlocking,
  }
)(App);

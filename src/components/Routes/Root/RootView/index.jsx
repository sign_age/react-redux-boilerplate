import React from 'react';
import { PropTypes } from 'prop-types';
import styles from './index.scss';

const RootView = ({ children }) => (
  <div id={styles.root} className="container">
    {children}
  </div>
);
RootView.propTypes = {
  children: PropTypes.node.isRequired,
};
export default RootView;

// TODO: STANDARDIZE MIN-HEIGHT
import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';
import { getRoutesBlocking } from '../../../ducks/routesBlocking';
import * as fromSnackbars from '../../../ducks/snackbars';
import RootView from './RootView';
import Blocking from '../../Blocking';
import RootSnackbar from './RootSnackbar';
import RootMenu from './RootMenu';

class Root extends Component {
  componentWillMount() {
    const { location: { pathname } } = this.props;
    this.prevPathname = pathname;
  }
  componentWillUpdate(nextProps) {
    const nextPathname = nextProps.location.pathname;
    const { location: { pathname } } = this.props;
    if (nextPathname !== pathname) {
      this.prevPathname = pathname;
    }
  }
  render() {
    const {
      children,
      firstSnackbar,
      removeFirstSnackbar,
      routesBlocking,
    } = this.props;
    return (
      <RootView>
        {routesBlocking && <Blocking />}
        <RootSnackbar
          firstSnackbar={firstSnackbar}
          removeFirstSnackbar={removeFirstSnackbar}
        />
        <RootMenu />
        {React.cloneElement(children, {
          prevPathname: this.prevPathname,
        })}
      </RootView>
    );
  }
}
Root.propTypes = {
  children: PropTypes.node.isRequired,
  firstSnackbar: PropTypes.object,
  location: PropTypes.object.isRequired,
  removeFirstSnackbar: PropTypes.func.isRequired,
  routesBlocking: PropTypes.bool.isRequired,
};
export default connect(
  (state) => ({
    firstSnackbar: fromSnackbars.getFirstSnackbar(state),
    routesBlocking: getRoutesBlocking(state),
  }), {
    removeFirstSnackbar: fromSnackbars.removeFirstSnackbar,
  }
)(Root);

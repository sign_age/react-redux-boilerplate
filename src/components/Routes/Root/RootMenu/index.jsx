import React from 'react';
import { Link } from 'react-router';
import brand from './brand.png';
import styles from './index.scss';

const RootMenu = () => (
  <nav id={styles.root} className="navbar navbar-default">
    <div className="container-fluid">
      <div className="navbar-header">
        <button
          type="button"
          className="navbar-toggle collapsed"
          data-toggle="collapse"
          data-target="#bs-example-navbar-collapse-1"
          aria-expanded="false"
        >
          <span className="sr-only">Toggle navigation</span>
          <span className="icon-bar" />
          <span className="icon-bar" />
          <span className="icon-bar" />
        </button>
        <div className="navbar-brand">
          <Link to="/">
            <img width="20" height="20" alt="Brand" src={brand} />
          </Link>
        </div>
      </div>
      <div className="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
        <ul className="nav navbar-nav">
          <li><Link to="/">Home</Link></li>
        </ul>
      </div>
    </div>
  </nav>
);
export default RootMenu;

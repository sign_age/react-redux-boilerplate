import React from 'react';
import { PropTypes } from 'prop-types';
import { hashHistory, IndexRoute, Route, Router } from 'react-router';
import { syncHistoryWithStore } from 'react-router-redux';
import Root from './Root';
import Items from './Items';
import Item from './Item';

const Routes = (props, { store }) => (
  <Router history={syncHistoryWithStore(hashHistory, store)}>
    <Route path="/" component={Root}>
      <IndexRoute component={Items} />
      <Route path="items/:id" component={Item} />
    </Route>
  </Router>
);
Routes.contextTypes = {
  store: PropTypes.object.isRequired,
};
export default Routes;

import React from 'react';
import { PropTypes } from 'prop-types';
import styles from './index.scss';

const ItemsView = ({ children }) => (
  <div id={styles.root}>
    {children}
  </div>
);
ItemsView.propTypes = {
  children: PropTypes.node.isRequired,
};
export default ItemsView;

import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { Field, reduxForm, SubmissionError, reset } from 'redux-form';
import { connect } from 'react-redux';
import * as fromRoutesBlocking from '../../../../ducks/routesBlocking';
import * as fromItems from '../../../../ducks/items';
import * as fromSnackbars from '../../../../ducks/snackbars';
import ValidatedTextInput from '../../../ValidatedTextInput';
import styles from './index.scss';

const ADD_ITEM_FORM = 'ADD_ITEM_FORM';
const ItemAdd = ({ handleSubmit, submitFailed, submitting, valid }) => (
  <form id={styles.root} onSubmit={handleSubmit}>
    <Field
      component={ValidatedTextInput}
      disabled={submitting}
      name="id"
      props={{ placeholder: 'id' }}
    />
    <div className="form-group">
      <Field
        className="form-control"
        component="input"
        disabled={submitting}
        name="description"
        placeholder="description"
        type="text"
      />
    </div>
    {submitFailed && !submitting &&
      <div className="alert alert-danger">Failed to add.</div>}
    <div className="form-group">
      <button
        disabled={!valid || submitting}
        type="submit"
        className="btn btn-primary"
      >Add</button>
    </div>
  </form>
);
ItemAdd.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  submitFailed: PropTypes.bool.isRequired,
  submitting: PropTypes.bool.isRequired,
  valid: PropTypes.bool.isRequired,
};
const ItemAddForm = reduxForm({
  form: ADD_ITEM_FORM,
  validate: values => {
    const errors = {};
    if (values.id === undefined) errors.id = '400';
    return errors;
  },
})(ItemAdd);
class ItemAddSubmit extends Component {
  constructor() {
    super();
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  handleSubmit({ id, description }) {
    const { addSnackbar, setRoutesBlocking, addItem, resetForm } = this.props;
    setRoutesBlocking(true);
    return addItem({
      id,
      description: description !== undefined ? description : '',
    })
      .then(() => {
        resetForm();
        setRoutesBlocking(false);
        addSnackbar({ message: 'Successfully added item.' });
      },
      (error) => {
        if (process.env.NODE_ENV !== 'production'
          && error.name !== 'ServerException') {
          window.console.log(error);
          return;
        }
        setRoutesBlocking(false);
        if (error.message === '409') throw new SubmissionError({ id: '409' });
        throw new SubmissionError({});
      }
    );
  }
  render() {
    return <ItemAddForm onSubmit={this.handleSubmit} />;
  }
}
ItemAddSubmit.propTypes = {
  addSnackbar: PropTypes.func.isRequired,
  setRoutesBlocking: PropTypes.func.isRequired,
  addItem: PropTypes.func.isRequired,
  resetForm: PropTypes.func.isRequired,
};
export default connect(
  null,
  {
    addSnackbar: fromSnackbars.addSnackbar,
    addItem: fromItems.addItem,
    resetForm: () => reset(ADD_ITEM_FORM),
    setRoutesBlocking: fromRoutesBlocking.setRoutesBlocking,
  }
)(ItemAddSubmit);

import React from 'react';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';
import * as fromReactRouterRedux from 'react-router-redux';
import { getItems } from '../../../ducks/items';
import ItemsView from './ItemsView';
import ItemsAdd from './ItemsAdd';
import ItemsList from './ItemsList';

const Items = ({ items, push }) => (
  <ItemsView>
    <ItemsAdd />
    <ItemsList
      items={items}
      push={push}
    />
  </ItemsView>
);
Items.propTypes = {
  items: PropTypes.array.isRequired,
  push: PropTypes.func.isRequired,
};
export default connect(
  (state) => ({
    items: getItems(state),
  }),
  {
    push: fromReactRouterRedux.push,
  }
)(Items);

import React from 'react';
import { PropTypes } from 'prop-types';

const Item = ({ item, push }) => (
  <a
    className="list-group-item"
    onClick={() => push(`/items/${item.id}`)}
  >{item.id}: {item.description}</a>
);
Item.propTypes = {
  item: PropTypes.object.isRequired,
  push: PropTypes.func.isRequired,
};
export default Item;

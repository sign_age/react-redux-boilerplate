import React from 'react';
import { PropTypes } from 'prop-types';
import Item from './Item';
import styles from './index.scss';

const ItemsList = ({ items, push }) => (
  <div id={styles.root} className="list-group">
    {items.map(item => (
      <Item
        item={item}
        key={item.id}
        push={push}
      />
    ))}
  </div>
);
ItemsList.propTypes = {
  items: PropTypes.array.isRequired, push: PropTypes.func.isRequired,
};
export default ItemsList;

import React from 'react';
import { PropTypes } from 'prop-types';
import styles from './index.scss';

const ItemView = ({ children }) => (
  <div id={styles.root}>
    {children}
  </div>
);
ItemView.propTypes = {
  children: PropTypes.node.isRequired,
};
export default ItemView;

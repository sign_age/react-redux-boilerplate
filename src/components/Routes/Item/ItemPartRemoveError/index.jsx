import React from 'react';
import styles from './index.scss';

export default () => (
  <div id={styles.root} className="alert alert-danger" role="alert">Failed to remove.</div>
);

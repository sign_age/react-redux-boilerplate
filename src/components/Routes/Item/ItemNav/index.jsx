import React from 'react';
import { PropTypes } from 'prop-types';
import styles from './index.scss';

const ItemNav = ({ push }) => (
  <div id={styles.root} className="form-group">
    <button
      className="btn btn-default"
      onClick={() => push('/')}
    >Back</button>
  </div>
);
ItemNav.propTypes = {
  push: PropTypes.func.isRequired,
};
export default ItemNav;

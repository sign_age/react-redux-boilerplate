import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { reduxForm, Field, SubmissionError } from 'redux-form';
import styles from './index.scss';

export const UPDATE_ITEM_FORM = 'UPDATE_ITEM_FORM';
const ItemUpdate = ({ handleSubmit, submitting, submitFailed }) =>
  <form id={styles.root} onSubmit={handleSubmit}>
    <div className="form-group">
      <Field
        component="input"
        name="id"
        type="text"
        disabled
        className="form-control"
      />
    </div>
    <div className="form-group">
      <Field
        component="input"
        name="description"
        type="text"
        placeholder="description"
        disabled={submitting}
        className="form-control"
      />
    </div>
    {submitFailed && !submitting &&
      <div className="alert alert-danger">Failed to update.</div>}
    <div className="form-group">
      <button
        type="submit"
        disabled={submitting}
        className="btn btn-primary"
      >Update</button>
    </div>
  </form>;
ItemUpdate.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  submitting: PropTypes.bool.isRequired,
  submitFailed: PropTypes.bool.isRequired,
};
const ItemUpdateForm = reduxForm({
  form: UPDATE_ITEM_FORM,
  enableReinitialize: true,
})(ItemUpdate);
class ItemUpdateSubmit extends Component {
  constructor() {
    super();
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  handleSubmit(item) {
    const {
      addSnackbar,
      setRoutesBlocking,
      updateItem,
      resetRemovePartError,
      resetAddItemPartForm,
      resetRemoveItemForm,
    } = this.props;
    resetRemovePartError();
    resetAddItemPartForm();
    resetRemoveItemForm();
    setRoutesBlocking(true);
    return updateItem(item)
      .then(
        () => {
          setRoutesBlocking(false);
          addSnackbar({ message: 'Succesfully updated item.' });
        },
        (error) => {
          if (process.env.NODE_ENV !== 'production'
            && error.name !== 'ServerException') {
            window.console.log(error);
            return;
          }
          setRoutesBlocking(false);
          throw new SubmissionError({});
        }
      );
  }
  render() {
    const { item } = this.props;
    return (
      <ItemUpdateForm
        onSubmit={this.handleSubmit}
        initialValues={item}
      />
    );
  }
}
ItemUpdateSubmit.propTypes = {
  addSnackbar: PropTypes.func.isRequired,
  item: PropTypes.object.isRequired,
  resetAddItemPartForm: PropTypes.func.isRequired,
  resetRemoveItemForm: PropTypes.func.isRequired,
  resetRemovePartError: PropTypes.func.isRequired,
  setRoutesBlocking: PropTypes.func.isRequired,
  updateItem: PropTypes.func.isRequired,
};
export default ItemUpdateSubmit;

import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { Field, reduxForm, SubmissionError, reset } from 'redux-form';
import { connect } from 'react-redux';
import ValidatedTextInput from '../../../ValidatedTextInput';
import styles from './index.scss';

export const ADD_ITEM_PART_FORM = 'ADD_ITEM_PART_FORM';
const ItemPartAdd = ({ handleSubmit, submitFailed, submitting, valid }) => (
  <form id={styles.root} onSubmit={handleSubmit}>
    <Field
      component={ValidatedTextInput}
      disabled={submitting}
      name="name"
      props={{ placeholder: 'name' }}
    />
    {submitFailed && !submitting &&
      <div className="alert alert-danger">Failed to add.</div>}
    <div className="form-group">
      <button
        disabled={!valid || submitting}
        type="submit"
        className="btn btn-primary"
      >Add</button>
    </div>
  </form>
);
ItemPartAdd.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  submitFailed: PropTypes.bool.isRequired,
  submitting: PropTypes.bool.isRequired,
  valid: PropTypes.bool.isRequired,
};
const ItemPartAddForm = reduxForm({
  form: ADD_ITEM_PART_FORM,
  validate: values => {
    const errors = {};
    if (values.name === undefined) errors.name = '400';
    return errors;
  },
})(ItemPartAdd);
class ItemPartAddSubmit extends Component {
  constructor() {
    super();
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  handleSubmit({ name }) {
    const {
      addSnackbar,
      item,
      addItemPart,
      resetForm,
      resetRemoveItemForm,
      resetRemovePartError,
      resetUpdateItemForm,
      setRoutesBlocking,
    } = this.props;
    resetRemovePartError();
    resetRemoveItemForm();
    resetUpdateItemForm();
    setRoutesBlocking(true);
    return addItemPart(item, {
      name,
    })
      .then(() => {
        resetForm();
        setRoutesBlocking(false);
        addSnackbar({ message: 'Succesfully added part.' });
      },
      (error) => {
        if (process.env.NODE_ENV !== 'production'
          && error.name !== 'ServerException') {
          window.console.log(error);
          return;
        }
        setRoutesBlocking(false);
        if (error.message === '409') throw new SubmissionError({ id: '409' });
        throw new SubmissionError({});
      }
    );
  }
  render() {
    return <ItemPartAddForm onSubmit={this.handleSubmit} />;
  }
}
ItemPartAddSubmit.propTypes = {
  addItemPart: PropTypes.func.isRequired,
  addSnackbar: PropTypes.func.isRequired,
  item: PropTypes.object.isRequired,
  resetForm: PropTypes.func.isRequired,
  resetRemovePartError: PropTypes.func.isRequired,
  resetRemoveItemForm: PropTypes.func.isRequired,
  resetUpdateItemForm: PropTypes.func.isRequired,
  setRoutesBlocking: PropTypes.func.isRequired,
};
export default connect(
  null,
  {
    resetForm: () => reset(ADD_ITEM_PART_FORM),
  }
)(ItemPartAddSubmit);

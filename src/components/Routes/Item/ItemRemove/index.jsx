import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { reduxForm, SubmissionError } from 'redux-form';
import styles from './index.scss';

export const DELETE_ITEM_FORM = 'DELETE_ITEM_FORM';
const ItemRemove = ({ handleSubmit, submitting,
  submitFailed }) =>
    <form id={styles.root} onSubmit={handleSubmit}>
      {submitFailed && !submitting &&
        <div className="alert alert-danger">Failed to remove.</div>}
      <div className="form-group">
        <button
          type="submit"
          disabled={submitting}
          className="btn btn-danger"
        >Remove</button>
      </div>
    </form>;
ItemRemove.propTypes = {
  handleSubmit: PropTypes.func.isRequired,
  submitting: PropTypes.bool.isRequired,
  submitFailed: PropTypes.bool.isRequired,
};
const ItemRemoveForm = reduxForm({
  form: DELETE_ITEM_FORM,
  enableReinitialize: true,
})(ItemRemove);
class ItemRemoveSubmit extends Component {
  constructor() {
    super();
    this.handleSubmit = this.handleSubmit.bind(this);
  }
  handleSubmit() {
    const {
      addSnackbar,
      item,
      push,
      removeItem,
      resetAddItemPartForm,
      resetRemovePartError,
      resetUpdateItemForm,
      setRoutesBlocking,
    } = this.props;
    resetAddItemPartForm();
    resetUpdateItemForm();
    resetRemovePartError();
    setRoutesBlocking(true);
    return removeItem(item.id)
      .then(
        () => {
          setRoutesBlocking(false);
          push('/');
          addSnackbar({ message: 'Succesfully removed item.' });
        },
        (error) => {
          if (process.env.NODE_ENV !== 'production'
            && error.name !== 'ServerException') {
            window.console.log(error);
            return;
          }
          setRoutesBlocking(false);
          throw new SubmissionError({});
        }
      );
  }
  render() {
    return (
      <ItemRemoveForm onSubmit={this.handleSubmit} />
    );
  }
}
ItemRemoveSubmit.propTypes = {
  addSnackbar: PropTypes.func.isRequired,
  item: PropTypes.object,
  push: PropTypes.func.isRequired,
  removeItem: PropTypes.func.isRequired,
  resetAddItemPartForm: PropTypes.func.isRequired,
  resetRemovePartError: PropTypes.func.isRequired,
  resetUpdateItemForm: PropTypes.func.isRequired,
  setRoutesBlocking: PropTypes.func.isRequired,
};
export default ItemRemoveSubmit;

import React from 'react';
import { PropTypes } from 'prop-types';
import ItemPart from './ItemPart';
import styles from './index.scss';

const ItemPartList = ({
  addSnackbar,
  itemParts,
  removePart,
  resetAddItemPartForm,
  resetRemoveItemForm,
  resetUpdateItemForm,
  setRoutesBlocking,
}) => (
  <ul id={styles.root} className="list-group">
    {itemParts.map(itemPart => (
      <ItemPart
        addSnackbar={addSnackbar}
        itemPart={itemPart}
        key={itemPart.id}
        removePart={removePart}
        resetAddItemPartForm={resetAddItemPartForm}
        resetRemoveItemForm={resetRemoveItemForm}
        resetUpdateItemForm={resetUpdateItemForm}
        setRoutesBlocking={setRoutesBlocking}
      />
    ))}
  </ul>
);
ItemPartList.propTypes = {
  addSnackbar: PropTypes.func.isRequired,
  itemParts: PropTypes.array.isRequired,
  removePart: PropTypes.func.isRequired,
  resetAddItemPartForm: PropTypes.func.isRequired,
  resetRemoveItemForm: PropTypes.func.isRequired,
  resetUpdateItemForm: PropTypes.func.isRequired,
  setRoutesBlocking: PropTypes.func.isRequired,
};
export default ItemPartList;

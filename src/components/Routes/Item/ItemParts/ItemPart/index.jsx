import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import ItemPartView from './ItemPartView';

class ItemPart extends Component {
  constructor() {
    super();
    this.handleRemoveClick = this.handleRemoveClick.bind(this);
  }
  handleRemoveClick() {
    const {
      addSnackbar,
      itemPart,
      removePart,
      resetAddItemPartForm,
      resetRemoveItemForm,
      resetUpdateItemForm,
      setRoutesBlocking,
    } = this.props;
    resetAddItemPartForm();
    resetUpdateItemForm();
    resetRemoveItemForm();
    setRoutesBlocking(true);
    removePart(itemPart.id)
      .then(() => {
        setRoutesBlocking(false);
        addSnackbar({ message: 'Succesfully deleted part.' });
      },
      (error) => {
        if (process.env.NODE_ENV !== 'production'
          && error.name !== 'ServerException') {
          window.console.log(error);
          return;
        }
        setRoutesBlocking(false);
      });
  }
  render() {
    const {
      itemPart,
    } = this.props;
    return (
      <ItemPartView
        handleRemoveClick={this.handleRemoveClick}
        itemPart={itemPart}
      />
    );
  }
}
ItemPart.propTypes = {
  addSnackbar: PropTypes.func.isRequired,
  itemPart: PropTypes.object.isRequired,
  removePart: PropTypes.func.isRequired,
  resetAddItemPartForm: PropTypes.func.isRequired,
  resetRemoveItemForm: PropTypes.func.isRequired,
  resetUpdateItemForm: PropTypes.func.isRequired,
  setRoutesBlocking: PropTypes.func.isRequired,
};
export default ItemPart;

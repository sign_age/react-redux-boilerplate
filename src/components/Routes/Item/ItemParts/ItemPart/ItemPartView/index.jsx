import React from 'react';
import { PropTypes } from 'prop-types';
import styles from './index.scss';

const ItemPartView = ({
  handleRemoveClick,
  itemPart,
}) => (
  <li
    className="list-group-item"
  >
    <div className="media">
      <div className="media-body">
        {itemPart.name}
      </div>
      <div className="media-right">
        <span
          className={`glyphicon glyphicon-remove ${styles.remove}`}
          aria-hidden="true"
          onClick={handleRemoveClick}
        />
      </div>
    </div>
  </li>
);
ItemPartView.propTypes = {
  handleRemoveClick: PropTypes.func.isRequired,
  itemPart: PropTypes.object.isRequired,
};
export default ItemPartView;

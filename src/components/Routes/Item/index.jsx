import React, { Component } from 'react';
import { PropTypes } from 'prop-types';
import { connect } from 'react-redux';
import * as fromReactRouterRedux from 'react-router-redux';
import { reset } from 'redux-form';
import * as fromRoutesBlocking from '../../../ducks/routesBlocking';
import * as fromItems from '../../../ducks/items';
import * as fromParts from '../../../ducks/parts';
import * as fromSnackbars from '../../../ducks/snackbars';
import ItemView from './ItemView';
import ItemNav from './ItemNav';
import ItemUpdate, { UPDATE_ITEM_FORM } from './ItemUpdate';
import ItemPartAdd, { ADD_ITEM_PART_FORM } from './ItemPartAdd';
import ItemParts from './ItemParts';
import ItemPartRemoveError from './ItemPartRemoveError';
import ItemRemove, { REMOVE_ITEM_FORM } from './ItemRemove';

class Item extends Component {
  componentDidMount() {
    const { resetRemovePartError } = this.props;
    resetRemovePartError();
  }
  render() {
    const {
      addItemPart,
      addSnackbar,
      item,
      itemParts,
      push,
      removeItem,
      removePart,
      removePartErrorMessage,
      resetAddItemPartForm,
      resetRemoveItemForm,
      resetRemovePartError,
      resetUpdateItemForm,
      setRoutesBlocking,
      updateItem,
    } = this.props;
    return (
    item !== undefined &&
      <ItemView>
        <ItemNav
          push={push}
        />
        <ItemUpdate
          addSnackbar={addSnackbar}
          item={item}
          resetAddItemPartForm={resetAddItemPartForm}
          resetRemoveItemForm={resetRemoveItemForm}
          resetRemovePartError={resetRemovePartError}
          setRoutesBlocking={setRoutesBlocking}
          updateItem={updateItem}
        />
        <ItemRemove
          addSnackbar={addSnackbar}
          item={item}
          push={push}
          removeItem={removeItem}
          resetAddItemPartForm={resetAddItemPartForm}
          resetRemovePartError={resetRemovePartError}
          resetUpdateItemForm={resetUpdateItemForm}
          setRoutesBlocking={setRoutesBlocking}
        />
        <ItemPartAdd
          addSnackbar={addSnackbar}
          addItemPart={addItemPart}
          item={item}
          resetRemoveItemForm={resetRemoveItemForm}
          resetRemovePartError={resetRemovePartError}
          resetUpdateItemForm={resetUpdateItemForm}
          setRoutesBlocking={setRoutesBlocking}
        />
        {removePartErrorMessage !== null &&
          <ItemPartRemoveError />}
        <ItemParts
          addSnackbar={addSnackbar}
          itemParts={itemParts}
          removePart={removePart}
          resetRemoveItemForm={resetRemoveItemForm}
          resetUpdateItemForm={resetUpdateItemForm}
          resetAddItemPartForm={resetAddItemPartForm}
          setRoutesBlocking={setRoutesBlocking}
        />
      </ItemView>
    );
  }
}
Item.propTypes = {
  addItemPart: PropTypes.func.isRequired,
  addSnackbar: PropTypes.func.isRequired,
  item: PropTypes.object,
  itemParts: PropTypes.array.isRequired,
  push: PropTypes.func.isRequired,
  removeItem: PropTypes.func.isRequired,
  removePart: PropTypes.func.isRequired,
  removePartErrorMessage: PropTypes.string,
  resetAddItemPartForm: PropTypes.func.isRequired,
  resetRemoveItemForm: PropTypes.func.isRequired,
  resetRemovePartError: PropTypes.func.isRequired,
  resetUpdateItemForm: PropTypes.func.isRequired,
  setRoutesBlocking: PropTypes.func.isRequired,
  updateItem: PropTypes.func.isRequired,
};
export default connect(
  (state, ownProps) => ({
    item: fromItems.getItem(state, ownProps.params.id),
    itemParts: fromItems.getItemParts(state, ownProps.params.id),
    removePartErrorMessage: fromParts.getRemovePartErrorMessage(state),
  }),
  {
    addItemPart: fromItems.addItemPart,
    addSnackbar: fromSnackbars.addSnackbar,
    push: fromReactRouterRedux.push,
    removeItem: fromItems.removeItem,
    removePart: fromParts.removePart,
    resetAddItemPartForm: () => reset(ADD_ITEM_PART_FORM),
    resetRemoveItemForm: () => reset(REMOVE_ITEM_FORM),
    resetRemovePartError: fromParts.resetRemovePartError,
    resetUpdateItemForm: () => reset(UPDATE_ITEM_FORM),
    setRoutesBlocking: fromRoutesBlocking.setRoutesBlocking,
    updateItem: fromItems.updateItem,
  }
)(Item);
